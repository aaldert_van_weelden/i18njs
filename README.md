# Standalone javascript i18n translator #

### Usage: ###

```
#!javascript

//just translate a string defined in the i18n language files by the provided key
var translated = lang.message('functionalDescriptionWithoutPlaceholders');

//you can also insert strings into the message. These strings are placed into placeholders like {0} , {1} etc.
var translated = lang.message('functionalDescriptionWithOnePlaceholder', 'insertValueOne', 'insertValueTwo');

//the inserted values can also be translated:
var translated = lang.message('functionalDescriptionWithTwoPlaceholders', lang.message('translateInsertValueOne'), lang.message('translateInsertValueTwo'));

//you can also use the dot-notation as a shortcut after calling lang.init(). However palceholders cannot be used then
var translated = lang.translate.functionalDescriptionWithoutPlaceholders;

//you can also translate application exception message strings like "AUTHENTICATION EXCEPTION: No access allowed to the current platform".
//the message is transformed into a valid key like AUTHENTICATION_EXCEPTION_NO_ACCESS_ALLOWED_TO_THE_CURRENT_PLATFORM by removing illegal chars "|&;:$%@"<>()+,.'" and replace spaces by underscores
var translated = lang.translate.functionalDescriptionWithoutPlaceholders;
```