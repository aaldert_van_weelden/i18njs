var i18n_NL = (function(){
	
	var lang = {
		functionalDescriptionWithoutPlaceholders : 'Dit is een vertaling zonder placeholders ',
		functionalDescriptionWithOnePlaceholder : 'Dit is een vertaling met één placeholder : {0}, dat is alles',
		functionalDescriptionWithTwoPlaceholders : 'Dit is een vertaling met twee placeholders. Nummer één: {0},   en dit is nummer twee: {1}',
		
		translateInsertValueOne : '--Dit is een ingevoegde vertaalde substring nummer 1--',
		translateInsertValueTwo : '--Dit is een ingevoegde vertaalde substring nummer 2--',
		
		dotnotation : 'Deze vertaling is aangeroepen door middel van de dot-notatie',
	};
	
/* do not edit below this line ---------------------------------------------------------------*/
	return {	
		message : function(key){
			for(name in lang){
				if(name===key){
					return lang[key];	
				}
			}
		},
		
		translations : lang
	};
	
})();