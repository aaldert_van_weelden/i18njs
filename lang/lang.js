/** 
 * JS i18n translation  base class
 * 
 * Usage:
 * 
 * //insert strings into the placeholders			
 * var myMessage0= lang.message('functionalDescriptionWithoutPlaceholders');
 * var myMessage1= lang.message('functionalDescriptionWithOnePlaceholder','insertValueOne');
 * var myMessage2= lang.message('functionalDescriptionWithTwoPlaceholders','insertValueOne','insertValueTwo');		
 * var myMessage3= lang.message('functionalDescriptionWithTwoPlaceholders',lang.message('translateInsertValueOne'), lang.message('translateInsertValueTwo'));
 * 
 * //using the dot notation after initialisation
 * var myMessage4= lang.message.dotnotation;
 * 
 * //using application exception messages as key:
 * var myMessage5= lang.message(error.myExceptionMessage);
 * 
 * @author Aaldert van Weelden
 */
var lang = (function(){
	
	var key=null;
	
	var  _stringFormat = function( text ){
		
		if(!text){
			return key;
			//throw 'Key ['+key+'] to translate not found in language file i18n['+LANG+'].js';
		}
        if ( arguments.length <= 1 ){
            return text;
        }
        var tokenCount = arguments.length - 2;
        for( var token = 0; token <= tokenCount; token++ ){
            text = text.replace( new RegExp( "\\{" + token + "\\}", "gi" ),arguments[ token + 1 ] );
        }
        return text;
    };
	
	return {
			
			/**
			 * Dot notation. Please initialize first before using the dot notation
			 * Usage: var translated = lang.translate.{key}
			 */
			translate : {},
			
			/**
			 * Expose the translation list as dot notation
			 */
			init : function(){
				if(typeof(LANG)=='undefined'){
					alert('Please set the global LANG variable before initializing the lang.js script');
					console.eror('LANG is undefined');	
				}
				this.translate = eval('i18n_'+LANG+'.translations');
			},
	
			message : function(){
				
				try{
					key = arguments[0];
					//set the default language here. Used if the global LANG variable is not set elsewhere
					if(typeof(LANG)=='undefined'){
						LANG ='EN';
						console.warn('LANG is undefined, setting default language:'+LANG);	
					}
					arguments[0] = eval('i18n_'+LANG+'.message(key)');
					return _stringFormat.apply(this,arguments);
				}catch(e){
					if (!window.console) window.console = {};
					if (!window.console.error) window.console.error = function (e) { alert(e);};
					console.warn(e);
				}
			},
			
			exception : function(){
				try{
					key = arguments[0];
					//set the default language here. Used if the global LANG variable is not set elsewhere
					if(typeof(LANG)=='undefined'){
						LANG ='EN';
						console.warn('LANG is undefined, setting default language:'+LANG);	
					}
					//process the key
					key = key.replace(/[|&;:$%@"<>()+,.']/g, "");
					key = key.replace(/ /g, "_");
					key = key.toUpperCase();
					arguments[0] = eval('i18n_'+LANG+'.message(key)');
					return _stringFormat.apply(this,arguments);
				}catch(e){
					if (!window.console) window.console = {};
					if (!window.console.error) window.console.error = function (e) { alert(e);};
					console.warn(e);
				}
			}
	};

})();